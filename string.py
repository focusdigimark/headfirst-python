#!/usr/bin/env python

print("""\
	The multiline string
	without /n
""")

# string

s1 = 'Py' 'thon'
print(s1)
s2 = (
	'string '
	'concat'
)
print(s2)

s3 = s1 + ' is a scripting language'

print(s3)
# letter

word = 'Python'
print(word[-1])

# slice
print (word[0:2])
print(word[:2] + word[2:])
print(len(word))

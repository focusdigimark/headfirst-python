#!/usr/env/bin python

squares = [1, 4, 9, 16, 25]
print(squares[1:3])
print(squares[-1])
print(squares[:])
print(squares + [3, 9, 27])

squares[0] = 2
print(squares)
squares.append('a')
print(squares)
squares[1:-1] = []
print(squares)

matrix = [['a','b','c'],[1,2,3]]
print(matrix[1][0])

# first steps
a, b = 0, 1
while a < 1000:
	print(a, end=',')
	a, b = b, a+b
#!/usr/env/bin python
import sys

# 1.Boolean

condition = False

if condition == True:
    print('OK men')
else:
    print('WTF')

if condition:
    print('ok')
else:
    print('eko')

# 2.String

str = 'Hello'
print(len(str))

if '':
    print('ok')

# 3.Number

# Integer no limit
num = 1234567891011121314151617181920
print(num)
# Float : 15

print(sys.float_info)

# 4. String

str = """Multiline
String
Sample
"""

print(str)
print(str[0:len("Multiline")])
print(str[len("Multiline"):])

# 5. Bytes

n = bytes(1)

print(n)

# 6. Lists

listA = [1, 'a', True, False, 1 + 2j, b'1']

print(listA)
for item in listA:
    print(item)
    print(type(item))
print(id(listA))
listA[0] = 'C'
print(id(listA))

# 7. Tuples : Immutable
listB = ['X','Y', 'Z']
tuppleA = ('A', 1, False, listB)

print (tuppleA)

print (tuppleA[1:])

# 8. Set

sample_set = set("Python data types")

print (sample_set)
color_set = {'red', 'green'}
color_set.add("blue")
print(color_set)

gender = frozenset({'male','female'})
print (gender)

if 'male' in gender:
	print ('valid input')

# 9. Dictionary

day_in_months= {
	'jan':31,
	'feb':28,
	'mar':31,
	'apr':30,
	'may':31,
	'jun':30,
	'jul':31,
	'aug':31,
	'sep':30,
	'oct':31,
	'nov':30,
	'dec':31
}

print(day_in_months['feb'])
day_in_months.update({'feb':29})
del day_in_months['feb']
print(day_in_months.keys(), day_in_months.values(), day_in_months.items())

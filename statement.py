#!/usr/bin/env python
PI = "123"
print(id(PI))
PI = "123"
print(id(PI))
print(type(PI))

# tuple

restaurants = ("A",)
restaurants += ("B", "C", "D")
print(type(tuple))
print(restaurants)

lists = ["X"]
lists += ['Y', 'Z']
print(type(lists))
print(lists)

months = [1,
          2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
          ]

print(months)

total = eval("2.5 + 3.5")
print(total)

subjects = [
    'Math',
    'Physics',
    'Biology',
    'Chemistry',
    'Science',
    'Literature'
]

print(subjects)

# The main purpose of this function is checking the number is odd or even


def is_odd_or_event(num):
    '''
The main purpose of this function is checking the number is odd or even
    '''
    print('The main purpose of this function is checking the number is odd or even')
    if num % 2 == 0:
        print(num, 'is even')
    else:
        print(num, 'is odd')


is_odd_or_event(1)
is_odd_or_event(2)

print ("Doc:", is_odd_or_event.__doc__)

#!/usr/bin/env python

# x = int(input("Please enter an integer: "))

# if x < 0:
# 	x = 0
# 	print('Negative changed to zero')
# elif x == 0:
# 	print('Zero')
# elif x == 1:
# 	print('Single')
# else:
# 	print('More')

words = ['cat', 'dogs', 'ab']

for w in words[:]:
	print(w, len(w))

for i in range(0,100,7):
	print(i)

for n in range(2,10):
	print(n)
	if(n == 6):		
		break

def fib(n = 2):
	a, b = 0, 1
	for i in range(1,n):
		print(a, end=' ')
		a, b = b, a+b
	print()
f = fib
f(10)
f()

def the_list(a, l=None):
	if(l is None):
		l = []
	l.append(a)
	return l

print(the_list(1))
print(the_list(2))

def the_faceshop(name, address='', is_new=False):
	print(name + ' is at ' + address + ' is ' + ('a new' if is_new else 'an old') + 'shop')

the_faceshop(is_new=False,address='D2 Street',name="the Loki")

def build_url(*args, sep='/'):
	return sep.join(args)

print(build_url('pages','about','me',sep='/'))

def the_unpack(first_name, last_name, age):
	print("{0} {1} is {2}".format(first_name, last_name, age))
d = {"first_name": 'SOn', "last_name": 'Nguyen', "age":31}
the_unpack(**d)

def total_price(vat):
	return lambda x: x * (1+vat)
total = total_price(0.1)
print(total(10000))

songs = []
songs.append({ "id":1, "name": 'Lạc Trôi', "singer": "Sơn Tùng", "views":10000, "downloads":9999, "pubDate":"2018-05-03" })
songs.append({ "id":2, "name": 'Bống bống bang bang', "singer": "ST", "views":9999, "downloads":10000, "pubDate": "2018-04-29" })
songs.append({ "id":3, "name": 'Ngắm hoa lệ rơi', "singer": "Khai Phong", "views":9998, "downloads":10001, "pubDate": "2018-05-29" })
key = 'downloads'
songs.sort(reverse=True,key=lambda song: song[key])
print(songs)

usertypes = ('sadmin','admin','user')

print(usertypes[0])

user = {
	"name": "lee",
	"type": usertypes[0]
}

print(user['name'] if user.get('name') else '')